// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-migrate-1.2.1
//= require custom
//= require contact_form
//= require jquery.carouFredSel
//= require jquery.countdown
//= require jquery.isotope.min
//= require jquery.noconflict
//= require mapmarker.jquery
//= require respond
//= require modernizr
//= require jquery.iosslider.kalypso
//= require jquery.iosslider.min


$(document).ready(function () {

    $('.iosSlider').iosSlider({
        snapToChildren: true,
        desktopClickDrag: true,
        keyboardControls: true,
        navNextSelector: $('.next'),
        navPrevSelector: $('.prev'),
        navSlideSelector: $('.selectors .item'),
        scrollbar: true,
        scrollbarContainer: '#slideshow .scrollbarContainer',
        scrollbarMargin: '0',
        scrollbarBorderRadius: '4px',
        onSlideComplete: slideComplete,
        onSliderLoaded: function (args) {
            var otherSettings = {
                hideControls: true, // Bool, if true, the NAVIGATION ARROWS will be hidden and shown only on mouseover the slider
                hideCaptions: false  // Bool, if true, the CAPTIONS will be hidden and shown only on mouseover the slider
            }
            sliderLoaded(args, otherSettings);
        },
        onSlideChange: slideChange,
        infiniteSlider: true,
        autoSlide: true
    });

});

